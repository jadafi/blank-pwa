<?php
$home = "/";
$top_dir = $_SERVER['DOCUMENT_ROOT'].$home;
error_reporting(E_ALL);
ini_set('display_errors', 'on');
?>

<html lang="en">
<head>
  <?php include $top_dir.'assets/html/head.php'; ?>
</head>

<body class="fullscreen">
  <?php include $top_dir.'assets/html/header.php'; ?>
  <div class="container">
    <h2 class="welcome">Hallöle</h2>
  </div> <!-- end container -->
  <script type="module" src="<?= $home ?>assets/js/landing.js"></script>
</body>
</html>
